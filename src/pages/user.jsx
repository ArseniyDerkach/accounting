import Counter from "../components/counter";

export function User(props) {
  return <>
    <h1>User Info</h1>
    <button onClick={()=> props.onClick()}>click me!</button>
    <Counter/>
  </>
}