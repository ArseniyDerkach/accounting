export default function TableRow({id, title, amount, type, date}) {
  const formattedDate = new Date(date);
  return <tr data-testid={`table-row-${id}`} key={id}>
  <td>{title}</td>
  <td>${amount}</td>
  <td style={{fontStyle: 'italic',color: type === 'debit' ? '#007700' : '#770000'}}>{type}</td>
  <td>{`${formattedDate.getDate()}.${formattedDate.getMonth()+1 < 10 ? '0'+(formattedDate.getMonth()+1) : formattedDate.getMonth()+1}`}</td>
  <td>&times;</td>
</tr>
}