import {counterTypes} from "../types";

const initialState = {
    currentCounter: 0
}

export function counterReducer(state = initialState, action) {
    switch (action.type) {
        case counterTypes.increase:
            return {currentCounter: state.currentCounter + 1}
        case counterTypes.decrease:
            return {currentCounter: state.currentCounter - 1}
        case counterTypes.reset:
            return {currentCounter: 0}
        default:
            return state
    }
}