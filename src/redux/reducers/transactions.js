import { transactionsTypes } from "../types";

export function transactionsReducer(state = [], action) {
  switch(action.type) {
    case transactionsTypes.addTransaction: 
        return [...state, action.payload];
      case transactionsTypes.removeTransaction:
        return state.filter(item=> item.id !== action.payload.id)
    case transactionsTypes.getTransactions:
      return action.payload;
    default: 
    return state;
  }
}