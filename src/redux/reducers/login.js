import {loginTypes} from "../types";

export function loginReducer(state = {}, action) {
    switch( action.type) {
        case loginTypes.login:
            return {token: action.payload}
        default:
            return state
    }
}
