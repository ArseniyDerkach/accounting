import { transactionsTypes } from "../types";

export function addTransaction(data) {
  return {
    type: transactionsTypes.addTransaction,
    payload: data
  }
}

export function removeTransaction(id) {
  return {
    type: transactionsTypes.removeTransaction,
    payload: {id}
  }
}

function fillTransactions(data) {
  return {
    type: transactionsTypes.getTransactions,
    payload: data,
  }
}

export function getTransactionsAsync() {
  return async function (dispatch) {
    const data = await fetch('https://accounting-be.vercel.app/rows', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json());
    dispatch(fillTransactions(data))
  }
}